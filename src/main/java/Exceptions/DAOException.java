/*
 * OOP CA6
 * @author Aleksandrs Laivins - D00196024
 *
 */
package Exceptions;

import java.sql.SQLException;

public class DAOException extends SQLException {
    public DAOException() {
    }

    public DAOException(String aMessage) {
        super(aMessage);
    }
}
