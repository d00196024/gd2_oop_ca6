/*
 * OOP CA6
 * @author Aleksandrs Laivins - D00196024
 *
 */
package DAOs;

import Exceptions.DAOException;

public interface ITollDAOInterface
{
    public double findTollBill(String name) throws DAOException;
    // public double registerValidTollEvent(String name) throws DAOException;
}
