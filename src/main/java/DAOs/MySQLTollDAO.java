/*
 * OOP CA6
 * @author Aleksandrs Laivins - D00196024
 *
 */
package DAOs;

import Exceptions.DAOException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MySQLTollDAO extends MySQLDAO implements ITollDAOInterface
{
    @Override
    public double findTollBill(String name) throws DAOException
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        double toll = 0;

        try
        {
            con = this.getConnection();

            String query = "select sum(cost) from toll_event natural join customer_vehicle natural join customer natural join vehicle natural join vehicle_type_cost where customer_name = \""
                    +name+"\" and timestamp BETWEEN SUBDATE(CURDATE(), INTERVAL 1 MONTH) AND NOW();\n";
            System.out.println(query);
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            while(rs.next())
            {
                toll = rs.getInt("sum(cost)");
                //String reg = rs.getString("vehicle_registration");
                //Vehicle u = new Vehicle(reg);
                //vehicles.add(u);
            }

        }
        catch(SQLException e)
        {
            throw new DAOException("getVehicleList() " + e.getMessage());
        }
        finally
        {
            try
            {
                if(rs != null)
                {
                    rs.close();
                }
                if(ps != null)
                {
                    ps.close();
                }
                if(con != null)
                {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DAOException("findTollBill() " + e.getMessage());
            }
        }
        return toll;
    }
}
