/*
 * OOP CA6
 * @author Aleksandrs Laivins - D00196024
 *
 */
package DAOs;

import Exceptions.DAOException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MySQLVehicleDAO extends MySQLDAO implements IVehicleDAOInterface
{
    @Override
    public List<String> getVehicleList() throws DAOException
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<String> vehicles = new ArrayList<>();

        try
        {
            con = this.getConnection();

            String query = "select vehicle_registration from vehicle";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            while(rs.next())
            {
                String reg = rs.getString("vehicle_registration");
                vehicles.add(reg);
            }

        }
        catch(SQLException e)
        {
            throw new DAOException("getVehicleList() " + e.getMessage());
        }
        finally
        {
            try
            {
                if(rs != null)
                {
                    rs.close();
                }
                if(ps != null)
                {
                    ps.close();
                }
                if(con != null)
                {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DAOException("getVehicleList() " + e.getMessage());
            }
        }
        return vehicles;
    }


}
