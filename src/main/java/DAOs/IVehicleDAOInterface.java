/*
 * OOP CA6
 * @author Aleksandrs Laivins - D00196024
 *
 */
package DAOs;

import DTO.Vehicle;
import Exceptions.DAOException;

import java.util.List;

public interface IVehicleDAOInterface
{

    //vehicle object or json??
    public List<String> getVehicleList() throws DAOException;

}
