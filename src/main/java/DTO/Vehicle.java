/*
 * OOP CA6
 * @author Aleksandrs Laivins - D00196024
 *
 */
package DTO;

import java.util.ArrayList;
import java.util.List;

public class Vehicle
{
    //list made with class so it can be replaced with json easily

    private List<String> regList;

    public Vehicle()
    {
        regList = new ArrayList<>();
    }

    public void addVehicleReg(List<String> regIn)
    {
        regList = regIn;
    }

    public List<String> getVehicleList()
    {
        return regList;
    }

}
