/*
 * OOP CA6
 * @author Aleksandrs Laivins - D00196024
 *
 */
package Client;

import Core.TollServiceDetails;
import JSON.ParseJSON;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.InputMismatchException;

public class TollServiceClient
{
    public static void main(String[] args) {
        Socket dataSocket = null;

        try
        {
            //Step 1: Establish a channel of communication
            dataSocket = new Socket("localhost", TollServiceDetails.LISTENING_PORT);

            //Step 2: Build the input and output objects
            OutputStream out = dataSocket.getOutputStream();
            PrintWriter output = new PrintWriter(new OutputStreamWriter(out));

            InputStream in = dataSocket.getInputStream();
            Scanner input = new Scanner(new InputStreamReader(in));

            Scanner keyboard = new Scanner(System.in);
            String message = "";
            while(!message.equals(TollServiceDetails.CLOSE))
            {
                displayMenu();
                int choice = getNumber(keyboard);
                String response = "";

                if(choice > 0 && choice < 4)
                {
                    switch(choice)
                    {
                        case 1:
                            message = TollServiceDetails.GET_REGISTERED_VEHICLES;
                            output.println(message);
                            output.flush();
                            response = input.nextLine();
                            System.out.println("["+response+"]");
                        break;
                        case 2:
                            message = TollServiceDetails.REGISTERED_VALID_TOLL_EVENT;
                            System.out.println("Enter name: ");
                            String name = keyboard.nextLine();
                            message = message.concat(TollServiceDetails.COMMAND_SEPARATOR);
                            message = message.concat(name);
                            output.println(message);
                            output.flush();
                            response = input.nextLine();
                            System.out.println("["+response+"]");
                        break;
                        case 3:
                            message = TollServiceDetails.CLOSE;
                            output.println(message);
                            output.flush();
                            break;
                        case 4:
                            message = TollServiceDetails.HEARTBEAT;
                            message = message.concat(TollServiceDetails.COMMAND_SEPARATOR);
                            message = message.concat(ParseJSON.BuildJSONObjectString("PacketType","Heartbeat"));
                            output.println(message);
                            output.flush();
                            response = input.nextLine();
                            System.out.println("["+response+"]");
                    }
                    if(response.equals(TollServiceDetails.UNRECOGNISED))
                    {
                        System.out.println("Sorry, that command is not recognised");
                    }
                 }
            else
            {
                System.out.println("Please select a number from the menu");
                displayMenu();
            }
        }
        System.out.println("Thank you for using the Vehicle Toll Service");
    }
        catch(UnknownHostException uhe)
    {
        System.out.println(uhe.getMessage());
    }
        catch(IOException ioe)
    {
        System.out.println(ioe.getMessage());
    }
        finally
    {
        try
        {
            if(dataSocket != null)
            {
                dataSocket.close();
            }
        }
        catch(IOException ioe)
        {
            System.out.println("Unable to disconnect " + ioe.getMessage());
            System.exit(1);
        }
    }
}

    public static void displayMenu()
    {
        System.out.println("1) View Vehicle Registrations");
        System.out.println("2) View Toll Bill By Name");
        System.out.println("3) End");
        System.out.println("4) Send Heartbeat");
    }

    public static int getNumber(Scanner keyboard)
    {
        boolean numberEntered = false;
        int number = 0;
        while(!numberEntered)
        {
            try
            {
                number = keyboard.nextInt();
                numberEntered = true;
            }
            catch(InputMismatchException ime)
            {
                System.out.println("Please enter a number");
                keyboard.nextLine();
            }
        }
        keyboard.nextLine();
        return number;
    }
}
