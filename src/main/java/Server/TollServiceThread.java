/*
 * OOP CA6
 * @author Aleksandrs Laivins - D00196024
 *
 */
package Server;

import Core.TollServiceDetails;
import DAOs.*;
import Exceptions.DAOException;

import javax.json.Json;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.*;

public class TollServiceThread extends Thread
{
    private Socket dataSocket;
    private Scanner input;
    private PrintWriter output;
    private int number;
    private String name;

    public TollServiceThread(ThreadGroup group, String name, Socket dataSocket, int number)
    {
        super(group, name);

        try
        {
            this.dataSocket = dataSocket;
            this.number = number;
            input = new Scanner(new InputStreamReader((this.dataSocket.getInputStream())));
            output = new PrintWriter(this.dataSocket.getOutputStream(), true);
        }
        catch(IOException ioe)
        {
            System.out.println(ioe.getMessage());
        }
    }

    @Override
    public void run()
    {
        String incomingMessage = "";
        String response;

        try
        {
            while(!incomingMessage.equals(TollServiceDetails.CLOSE))
            {
                response = null;

                //take input from the client
                incomingMessage = input.nextLine();
                System.out.println("Received message: " + incomingMessage);

                //Break the incomingMessage into components
                String[] components = incomingMessage.split(TollServiceDetails.COMMAND_SEPARATOR);

                System.out.println("Components::");
                System.out.println(components.length);
                for (String s:components
                     ) {
                    System.out.println(s);
                }
                System.out.println("-------------");



                if(components[0].equals(TollServiceDetails.GET_REGISTERED_VEHICLES))
                {
                    //System.out.println("components: "+components.toString());
                    //response = generateResponse(Integer.parseInt(components[1]));
                    List<String> p = generatePlates();
                    StringBuilder out = new StringBuilder();

                    for (int i = 0; i < p.size(); i++) {
                        out.append(p.get(i));

                        if(i!=(p.size()-1))
                        {
                            out.append(", ");
                        }
                    }
                    response = out.toString();
                }
                else if(components[0].equals(TollServiceDetails.REGISTERED_VALID_TOLL_EVENT))
                {
                    //System.out.println("components: "+components.toString());
                    //System.out.println("TOLL EVENT");
                    double toll = -1.00;
                    System.out.println(components[1]);
                    name = components[1];
                    toll = generateTollByName(name);
                    //System.out.println(toll);
                    response = ""+toll;
                    //response = "1";
                }
                else if(components[0].equals(TollServiceDetails.HEARTBEAT))
                {
                    JsonBuilderFactory factory = Json.createBuilderFactory(null);

                    JsonObject jsonRootObject
                            = Json.createObjectBuilder()
                            .add("PacketType", "Heartbeat response")
                            .build();

                    String message = jsonRootObject.toString();

                    response = message;
                    //response = "1";
                }
                else
                {
                    response = TollServiceDetails.UNRECOGNISED;
                }

                System.out.println("Sending response: " + response);
                output.println(response);
            }
        }
        catch(NoSuchElementException nse)
        {
            System.out.println(nse.getMessage());
        }
        finally
        {
            try
            {
                System.out.println("\n Closing connection with client #" + number + "...");
                if(dataSocket != null)
                {
                    dataSocket.close();
                }
            }
            catch(IOException ioe)
            {
                System.out.println("Unable to disconnect: " + ioe.getMessage());
                System.exit(1);
            }
        }
    }

    public static double generateTollByName(String name)
    {
        double toll = 0;
        try {
            ITollDAOInterface ITollDao = new MySQLTollDAO();
            toll = ITollDao.findTollBill(name);
            //plates = IVehicleDao.getVehicleList();
        }catch(DAOException e)
        {
            System.out.println("generateTollByName() : " + e.getMessage());
        }

        return toll;
    }

    public static List<String> generatePlates()
    {
        List<String> plates = null;
        try {
            IVehicleDAOInterface IVehicleDao = new MySQLVehicleDAO();
            plates = IVehicleDao.getVehicleList();
        }catch(DAOException e)
        {
            System.out.println("generatePlates() : " + e.getMessage());
        }

        return plates;
    }
}
