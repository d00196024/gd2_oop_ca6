/*
 * OOP CA6
 * @author Aleksandrs Laivins - D00196024
 *
 */
package Core;

public class TollServiceDetails
{
    public static final int LISTENING_PORT = 50000;

    //Breaking characters
    public static final String COMMAND_SEPARATOR = "%";

    //Command strings
    public static final String CLOSE = "***CLOSE***";

    //
    //COMMAND
    public static final String HEARTBEAT = "HEARTBEAT";
    public static final String GET_REGISTERED_VEHICLES = "GET_REGISTERED_VEHICLES";
    public static final String REGISTER_VALID_TOLL_EVENT = "REGISTER_VALID_TOLL_EVENT";
    public static final String REGISTER_INVALID_TOLL_EVENT = "REGISTER_INVALID_TOLL_EVENT";

    //RESPONSE
    public static final String HEARTBEAT_RESPONSE = "HEARTBEAT_RESPONSE";
    public static final String RETURN_REGISTERED_VEHICLES = "RETURN_REGISTERED_VEHICLES";
    public static final String REGISTERED_VALID_TOLL_EVENT = "REGISTERED_VALID_TOLL_EVENT";
    public static final String REGISTERED_INVALID_TOLL_EVENT = "REGISTERED_INVALID_TOLL_EVENT";
    //
    //

    //Response strings
    public static final String UNRECOGNISED = "UNKNOWN_COMMAND";
}
