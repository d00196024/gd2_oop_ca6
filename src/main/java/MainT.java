/*
 * OOP CA6
 * @author Aleksandrs Laivins - D00196024
 *
 */
import DAOs.ITollDAOInterface;
import DAOs.IVehicleDAOInterface;
import DAOs.MySQLTollDAO;
import DAOs.MySQLVehicleDAO;
import Exceptions.DAOException;

import java.util.ArrayList;
import java.util.List;

public class MainT
{
    public static void main(String[] args) {

        // testing mysql queries

        IVehicleDAOInterface IVehicleDao = new MySQLVehicleDAO();

        List<String> plates = null;
        try{
            plates = IVehicleDao.getVehicleList();
        }
        catch(DAOException e)
        {
            e.getMessage();
        }


        for (String p:plates
             ) {
            System.out.println("["+p+"]");
        }

        System.out.println("------------------------------");

        ITollDAOInterface ITollDao = new MySQLTollDAO();
        double toll = -1;
        System.out.println(toll);
        try{
            toll = ITollDao.findTollBill("Aleksandrs Laivins");
        }
        catch(DAOException e)
        {
            e.getMessage();
        }

        System.out.println("Toll = "+toll );


    }
}
